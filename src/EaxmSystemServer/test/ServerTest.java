package EaxmSystemServer.test;

import EaxmSystemServer.server.ServerDao;
import EaxmSystemServer.server.ServerInitClose;
import EaxmSystemServer.server.ServerView;

import java.io.IOException;

public class ServerTest {
    public static void main(String[] args) {
        ServerInitClose sic = null;

        try {
//        1.声明serverInitClose类型的引用指向该类型的对象
            sic = new ServerInitClose();
//        2.调用成员方法实现服务器的初始化操作
            sic.serverInit();
//            4.声明Serverdao类型的引用指向该类型的对象
            ServerDao sd=new ServerDao();
//            3.声明severview类型的引用指向该类型的对象
            ServerView sv = new ServerView(sic,sd);
            sv.serverReceive();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
//        3.调用成员方法试下服务器的关闭操作
            try {
                sic.serverClose();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
