package EaxmSystemClient.model;

public class UserMessage implements java.io.Serializable {
    //    消息的类型代表具体的业务
    private String type;
    //    消息的具体内容
    private User user;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserMessage() {
    }

    public UserMessage(String type, User user) {
        this.type = type;
        this.user = user;
    }
}
